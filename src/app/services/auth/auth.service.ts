import { ApiService } from './../api/api.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface AuthState {
  loggedIn: boolean;
  tokenExpired: boolean;
}

@Injectable()
export class AuthService {
  private authState: AuthState = {
    loggedIn: false,
    tokenExpired: false
  };

  private onAuthChangedSubject = new BehaviorSubject<AuthState>(this.authState);
  private token: string = null;
  // private loggedIn = false;


  constructor(private api: ApiService) {
    this.token = localStorage.getItem('token');
    if (this.token) {
      this.authState.loggedIn = true;
      this.onAuthChangedSubject.next(this.authState);
    }
  }

  isLoggedIn() {
    return this.authState.loggedIn;
  }

  async login(mobile, password) {
    try {
      const response: any = await this.api.login(mobile, password).toPromise();
      this.token = response.access_token;
      localStorage.setItem('token', this.token);
      this.authState = {
        loggedIn: true,
        tokenExpired: false
      };
      this.onAuthChangedSubject.next(this.authState);
      this.authState = {
        loggedIn: true,
        tokenExpired: false
      };
      this.onAuthChangedSubject.next(this.authState);

      return true;
    } catch (error) {
      throw new Error('Failed to login.');
    }
    // this.loggedIn = true;
    // this.token = 'token-token';
    // localStorage.setItem('token', this.token);
  }

  logout(tokenExpired: boolean = false) {
    this.token = null;
    localStorage.removeItem('token');
    this.authState.loggedIn = false;
    this.authState.tokenExpired = tokenExpired;
    this.onAuthChangedSubject.next(this.authState);
  }

  getToken() {
    return this.token;
  }

  onAuthChange() {
    return this.onAuthChangedSubject;
  }

  onTokenExpired() {
    if (this.authState.loggedIn) {
      this.logout(true);
    }
  }
}
