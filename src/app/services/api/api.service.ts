
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  authUrl = 'http://localhost:3001';
  orderUrl = 'http://localhost:3000';


  constructor(private http: HttpClient) {

  }

  login(email: string, password: string) {
    return this.http.post(`${this.authUrl}/auth/login`, { email, password });
  }

  getOrderList() {
    return this.http.get(`${this.orderUrl}/orders/status`);
  }
  createOrder(productName: string) {
    return this.http.post(`${this.orderUrl}/orders`, { productName });
  }

  payOrder(id: string) {
    return this.http.put(`${this.orderUrl}/orders/pay/${id}`, undefined);
  }
  cancelOrder(id: string) {
    return this.http.put(`${this.orderUrl}/orders/cancel/${id}`, undefined);
  }




}
