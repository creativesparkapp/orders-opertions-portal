import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';


export enum SocketOutputEvent {
  message,
}

interface SocketEventParam {
  message: string;
}


/*
  Generated class for the SocketProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventService {

  constructor(private socket: Socket) { }


  listenDataChanges() {
    return this.socket
      .fromEvent('data-updated');
  }
}
