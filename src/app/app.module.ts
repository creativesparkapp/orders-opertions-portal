import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './pages/login/login.component';
import { BaseComponent } from './pages/base/base.component';
import { AuthGuard } from './guards/auth.guard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ApiService } from './services/api/api.service';
import { AuthService } from './services/auth/auth.service';
import { TokenInterceptor } from './http-interceptors/token.interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };


import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatDividerModule,
  MatTableModule,
  MatProgressBarModule,
  MatDialogModule,
  MatSnackBarModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatChipsModule
} from '@angular/material';
import { OrderComponent } from './pages/order/order.component';
import { NewOrderComponent } from './pages/order/new-order/new-order.component';
import { EventService } from './services/event/event.service';

export const appRoutes: Routes = [
  {
    path: '', canActivate: [AuthGuard], component: BaseComponent, children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'order', component: OrderComponent },
    ]
  },
  { path: 'login', component: LoginComponent },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    LoginComponent,
    BaseComponent,
    DashboardComponent,
    OrderComponent,
    NewOrderComponent,
  ],
  imports: [
    FormsModule,
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    FlexLayoutModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatDividerModule,
    MatTableModule,
    MatProgressBarModule,
    MatDialogModule,
    MatChipsModule,
    MatSnackBarModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [
    ApiService,
    AuthService,
    AuthGuard,
    EventService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}}
  ],
  entryComponents: [
    NewOrderComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
