import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.scss']
})
export class NewOrderComponent implements OnInit {

  constructor(private readonly api: ApiService, public dialogRef: MatDialogRef<NewOrderComponent>) { }
  productName: string;
  ngOnInit() {
  }

  submitOrder() {
    this.api.createOrder(this.productName).subscribe((res) => {
      this.dialogRef.close(res);
    });
  }

}
