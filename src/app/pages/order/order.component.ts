import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { MatDialog } from '@angular/material';
import { NewOrderComponent } from './new-order/new-order.component';
import { EventService, SocketOutputEvent } from 'src/app/services/event/event.service';

enum UserType {
  rider,
  driver
}

enum OrderStatusColor {
  created = 'gray',
  confirmed = 'blue',
  cancelled = 'red',
  delivered = 'green',
}

enum OrderStatus {
  created = 'created',
  confirmed = 'confirmed',
  cancelled = 'cancelled',
  delivered = 'delivered',
}



@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  order: any;
  displayedColumns: string[] = ['productName', 'id', 'status', 'action'];
  dataSource: any;
  userDetails: any;
  onLoading: boolean;

  constructor(private api: ApiService, public dialog: MatDialog, public event: EventService) { }

  ngOnInit() {
    this.getOrderList();
    this.event.listenDataChanges().subscribe(() => {
      console.log('data updated');
      
      this.getOrderList();
    });
  }


  getOrderType(userType) {
    return UserType[userType];
  }

  getOrderList() {
    this.onLoading = true;

    this.api.getOrderList().subscribe((orderList) => {
      console.log(orderList);

      this.dataSource = orderList;
      this.onLoading = false;
    }, err => {
      this.onLoading = false;

    });
  }

  viewUserDetails(user) {
    // this.api.getOrderList(user.userId).subscribe(userDetails => {

    //   this.onLoading = true;

    //   const dialogRef = this.dialog.open(UserDetailsComponent, {
    //     width: '700px',
    //     data: userDetails
    //   });
    //   this.onLoading = false;

    //   dialogRef.afterClosed().subscribe(result => {
    //     if (result) {
    //       this.getOrderList();

    //     }

    //   });

    // });
  }

  newOrder() {
    const dialogRef = this.dialog.open(NewOrderComponent, {
      width: '500px',
    });
    this.onLoading = false;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getOrderList();
      }
    });
  }

  cancelOrder(orderId) {
    this.api.cancelOrder(orderId).subscribe((res) => {
      this.getOrderList();
    });
  }
  payOrder(orderId) {
    this.api.payOrder(orderId).subscribe((res) => {
      this.getOrderList();
    });
  }

  getStatusColor(status: OrderStatus) {
    switch (status) {
      case OrderStatus.created:
        return OrderStatusColor.created;
      case OrderStatus.confirmed:
        return OrderStatusColor.confirmed;
      case OrderStatus.delivered:
        return OrderStatusColor.delivered;
      case OrderStatus.cancelled:
        return OrderStatusColor.cancelled;
    }
  }

}
