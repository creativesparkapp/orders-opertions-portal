import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  constructor(private router: Router) { }

  menuItem = [{
    name: 'Order', navLink: 'order', icon: 'shopping_cart'
  }];

  ngOnInit() {

    this.router.navigate(['order']);

  }

  onMenuClick(navLink) {
    this.router.navigate([navLink]);

  }
  onLogout(navLink) {
    this.router.navigate(['login']);
  }



}
