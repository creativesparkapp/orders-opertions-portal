import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  password: string;
  mobile: number;
  onLoading: boolean;

  constructor(private snackBar: MatSnackBar, private router: Router, private api: ApiService, private authService: AuthService) { }
  ngOnInit() {
  }

  onLogin() {
    this.authService.login(this.mobile, this.password).then((authorized) => {
      this.onLoading = true;
      if (authorized) {
        this.onLoading = false;
        this.router.navigate(['dashboard']);
      } else {

        this.onLoading = false;
      }
    }, err => {
      this.openSnackBar('Invalid username or password');
      this.onLoading = false;

    });

  }

  openSnackBar(message: string) {
    this.snackBar.open(message, undefined, {
      duration: 2000,
    });
  }

}
