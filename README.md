# Order-Operation-Portal (setel fullstack)

Web application buid with angular with socket io implementation 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Angular: https://cli.angular.io/
* Orders application: https://bitbucket.org/creativesparkapp/orders-application/src/master/
* Auth application: https://bitbucket.org/creativesparkapp/auth-app/src/master/
* Payment application: https://bitbucket.org/creativesparkapp/payments-application/src/master/

### Installing

* Clone the repo
* run 
```
	$ npm install
	$ npm run dev
```

## Test

* open url http://localhost:4200
* use auth application with swagger interface to register user
* login by using created account

## Built With

* [Angular](https://angular.io/) - The SPA (single page application) framework used


## Authors

* **Burhanuddin Helmy** *

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details